<h1 align="center">EGC-Assignment</h1>

## Built With

[![Express.js](https://img.shields.io/badge/Express.js-4.x-orange.svg?style=rounded-square)](https://expressjs.com/en/starter/installing.html)
[![Node.js](https://img.shields.io/badge/Node.js-v.12.13-green.svg?style=rounded-square)](https://nodejs.org/)

## Requirements

1. <a href="https://nodejs.org/en/download/">Node Js</a>
2. node_modules
3. <a href="https://www.getpostman.com/">Postman</a>
4. Web Server (ex. localhost)
5. <a href="https://vuejs.org/">Vue</a>

## How to run app ?

1. Open app's directory in CMD or Terminal
2. git clone https://github.com/NozomuCo/Project-EGC.git
3. Make new file a called **.env** for express in /be, set up first [here](#set-up-express-env-file)
4. Make new file a called **.env** for vue in /fe, set up first [here](#set-up-vue-env-file)
5. `docker-compose build`
6. `docker-compose up`
7. Open your app || `egc.nozomu.co`

## Set up express .env file on the server folder

Open .env file on your favorite code editor, and copy paste this code below :

```
BE_PORT=2000
```

## Set up vue .env file on the client folder

Open .env file on your favorite code editor, and copy paste this code below :

```
VUE_APP_API = http://localhost:2000
```
## Unit testing using JEST on the server folder

For unit testing on the server folder, execute the command

```
npm test
```

Sample output unit testing

```
> egc-be@1.0.0 test /Users/aryoprakarsa/Sites/egc/server
> jest

 PASS  ./thumbnail.test.js
  Thumbnail resizing test
    ✓ POST /thumbnail/resize (18 ms)

Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        1.375 s
Ran all test suites.
```
import axios from 'axios'

export default {
  state: {},
  mutations: {},
  actions: {
    resizing(context, payload) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${process.env.VUE_APP_API}/thumbnail/resize`, payload)
          .then(result => {
            resolve(result)
          })
          .catch(error => {
            console.log(error)
            reject(error)
          })
      })
    }
  },
  getters: {}
}

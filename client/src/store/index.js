import Vue from 'vue'
import Vuex from 'vuex'
import Resize from './modules/resize'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: { Resize },
  state: {
    name: 'Ilham'
  },
  mutations: {},
  actions: {},
  getters: {}
})

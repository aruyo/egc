const axios = require('axios')
const fs = require('fs')
const FormData = require('form-data')

describe('Thumbnail resizing test', () => {
  it('POST /thumbnail/resize', async () => {
    const data = new FormData()
    data.append('thumbnail', fs.createReadStream('./example.png'))
    axios
      .create({
        headers: data.getHeaders()
      })
      .post('http://localhost:2000/thumbnail/resize', data)
      .then((response) => {
        expect(response.status).to.equal(200)
        console.log(response)
      })
      .catch((error) => {
        if (error.response) {
          console.log(error.response)
        }
        console.log(error.message)
      })
  })
})

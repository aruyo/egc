const resizer = require('node-image-resizer')
const helper = require('../helper/response')
const fs = require('fs')
const path = require('path')

module.exports = {
  resizeThumbnail: async (req, res) => {
    try {
      // delete recent resize image
      const resizeDirectory = './thumbnail/'
      fs.readdir(resizeDirectory, (err, files) => {
        if (err) throw err

        for (const file of files) {
          fs.unlink(path.join(resizeDirectory, file), (err) => {
            if (err) throw err
          })
        }
      })

      // setup image size
      const setup = {
        all: {
          path: './thumbnail/',
          quality: 80
        },
        versions: [
          {
            prefix: 'big_',
            width: 50,
            height: 50
          },
          {
            prefix: 'medium_',
            width: 20,
            height: 20
          },
          {
            quality: 100,
            prefix: 'small_',
            width: 10,
            height: 10
          }
        ]
      }
      // resizing image
      const result = await resizer(
        `./thumbnailUpload/${req.file.filename}`,
        setup
      )
      // delete recent upload image
      const directory = './thumbnailUpload/'
      fs.readdir(directory, (err, files) => {
        if (err) throw err

        for (const file of files) {
          fs.unlink(path.join(directory, file), (err) => {
            if (err) throw err
          })
        }
      })
      return helper.response(res, 200, 'Success resizing your image', result)
    } catch (error) {
      console.log(error)
      return helper.response(res, 400, 'Bad request!', error)
    }
  }
}

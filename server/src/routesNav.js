const router = require('express').Router()
const thumbnail = require('./routes/thumbnail')

router.use('/thumbnail', thumbnail)

module.exports = router

const router = require('express').Router()
const { resizeThumbnail } = require('../controller/thumbnail')
const uploadThumbnail = require('../middleware/multer')

router.post('/resize', uploadThumbnail, resizeThumbnail)

module.exports = router

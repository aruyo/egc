const cors = require('cors')
const express = require('express')
const app = express()
app.use(cors())
app.use((request, response, next) => {
  response.header('Access-Control-Allow-Origin', '*')
  response.header(
    'Access-Control-Allow-Methods',
    'POST, GET, OPTIONS, PUT, DELETE'
  )
  response.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Request-With, Content-Type, Accept, Authorization'
  )
  next()
})

const morgan = require('morgan')
require('dotenv').config()
const routesNav = require('./src/routesNav')

app.use(morgan('dev'))
app.use(express.static('thumbnail'))

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use('/', routesNav)

app.get('*', (request, response) => {
  response.status(404).send('Path not found !')
})

app.listen(process.env.BE_PORT, () => {
  console.log(`Express app is listening on port ${process.env.BE_PORT}`)
})
